

Proposals and Application Specific Extensions
=============================================

Those might move to the Extensions after discussion. Application
developers are invited to fork the repository and make pull requests. At
some point it might be proposed for inclusion to the Extensions through
the Create mailing list.

.. toctree::
   :maxdepth: 2
   :caption: Currently available proposals are:
   
   animation
   embed-fonts
   filter-effects
   multiple-pages
   palette
   png-data-requirements
   undo-history
